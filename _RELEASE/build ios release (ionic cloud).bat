@echo off
SET version="FEHLER - Schliesse das Programm manuell"
for /f %%i in ('..\_BATCHES\_xpath.bat "..\config.xml" "//widget/@version"') do set version=%%i
echo Version (in config.xml): %version%
echo Taste drücken wenn die Version passt
PAUSE
CD ..
@echo on
CALL ionic cordova plugin rm cordova-plugin-console
CALL ionic package build ios --profile prod --release --prod
CALL ionic cordova plugin add cordova-plugin-console
PAUSE