CD ..
IF EXIST platforms\android\build\outputs\apk\android-release.apk DEL platforms\android\build\outputs\apk\android-release.apk
CALL ionic cordova plugin rm cordova-plugin-console
CALL ionic cordova build android --prod --release
SET Now=%Date:~8,2%%Date:~3,2%%Date:~0,2%-%Time:~0,2%%Time:~3,2%%Time:~6,2%
MOVE platforms\android\build\outputs\apk\android-release.apk _RELEASE\%Now%.apk
CALL ionic cordova plugin add cordova-plugin-console
PAUSE