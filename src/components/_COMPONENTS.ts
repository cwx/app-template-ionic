import {Directives} from '../directives/_DIRECTIVES';

import {CwxSelectSearchable} from './cwx-select-searchable/cwx-select-searchable';
import {CwxSelectSearchableContent} from './cwx-select-searchable/content';
import {CwxKeyboardSearchbar} from './cwx/keyboard-searchbar/keyboard-searchbar';
import {CwxImg} from './cwx/img';
import {CwxHeader} from './cwx/header';

export const Components = {
	list: <Array<any>> [

		// CWX ---------------------------------------------------------------------------------------------------------
		CwxSelectSearchableContent,
		CwxKeyboardSearchbar,
		CwxImg,
		CwxHeader

	],
	listNoEntry: <Array<any>> [
		CwxSelectSearchable
	]
};

Directives.list.push(CwxSelectSearchable);
