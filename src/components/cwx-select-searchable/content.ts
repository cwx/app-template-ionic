import {Component} from '@angular/core';
import {NavParams, ViewController} from 'ionic-angular';

import {Config} from '../../services/config';

@Component({
	templateUrl: 'content.html'
})
export class CwxSelectSearchableContent {
	public title :string;

	public options :Array<any>;
	public filteredOptions :Array<any>;
	public selectedOption :any;
	public initialSelectedOption :any;
	public optionLabelProperty :string;

	public onSelect :() => void;
	public cancelButtonText :string;

	public searchPlaceholder :string = "Search";

	constructor (
		public navParams :NavParams,
		public viewCtrl :ViewController,
		public config: Config
	) {
		this.title = this.navParams.get('title');
		this.cancelButtonText = (this.navParams.get('cancelButtonText') || this.cancelButtonText);
		this.searchPlaceholder = (this.navParams.get('searchPlaceholder') || this.searchPlaceholder);

		this.optionLabelProperty = this.navParams.get('optionLabelProperty');

		this.onSelect = () => {
			// check is necessary because list hits "onSelect" every time it renders (onInit, onNgIf=true)
			if (this.selectedOption != this.initialSelectedOption) {
				this.navParams.get('onSelect')(this.selectedOption);
				this.viewCtrl.dismiss();
			}
		};
	}

	// delay init of the virtual list cause the its layout gets corrupt otherwise on slow devices
	ionViewDidEnter () {
		this.filteredOptions = this.options = this.navParams.get('options');
		this.initialSelectedOption = this.selectedOption = this.navParams.get('selectedOption');
	}

	isSelected (option :any) :boolean {
		return (option == this.selectedOption);
	}

	onSearch (search :string = '') {
		search = search.toLowerCase();
		this.filteredOptions = this.options.filter((item) => {
			return (item.name.toLowerCase().indexOf(search) > -1);
		});
	}
}