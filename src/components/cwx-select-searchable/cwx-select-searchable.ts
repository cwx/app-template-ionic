import {Directive, ElementRef, Input} from '@angular/core';
import {Select, Option, ModalController} from 'ionic-angular';

import {CwxSelectSearchableContent} from './content'

// the code in this directive is inspired by ionics select component - https://github.com/driftyco/ionic/blob/master/src/components/select/select.ts

@Directive({
	selector: 'ion-select[cwx-searchable]'
})
export class CwxSelectSearchable {
	@Input() options :Array<any>;
	@Input('ngModel') selectedOption :any;
	@Input('option-label-property') optionLabelProperty :string;
	@Input('search-placeholder') searchPlaceholder :string;

	public select :any; // type is "Select" but we're overwriting and Typescript does not allow that

	constructor (
		public originalSelect :Select,
		public modalCtrl: ModalController
	) {
		// see declaration of this.select
		this.select = originalSelect;
	}

	// overwrite the selects own methods
	ngAfterContentInit () {
		this.select.open = () => this.open();

		this.select._inputShouldChange = (val: any) => {
			this.onBeforeInputChanges(val);
			return true;
		}
	}

	open () {
		if (this.select.isFocus() || this.select._disabled) {
			return;
		}

		// open modal
		{
			let modal = this.modalCtrl.create(CwxSelectSearchableContent, {
				title: ((this.select.selectOptions && this.select.selectOptions.title) || (this.select._item && this.select._item.getLabelText())),
				selectedOption: this.selectedOption,
				options: this.options,
				optionLabelProperty: this.optionLabelProperty,
				onSelect: (newSelectedOption) => {
					this.selectedOption = newSelectedOption;
					this.select.value = this.selectedOption;
				},
				cancelButtonText: this.select.cancelText,
				searchPlaceholder: this.searchPlaceholder
			});
			modal.onDidDismiss(data => {
				this.select._fireBlur();
			});
			modal.present();
			this.select._fireFocus();
		}
	}

	// push selected options into the select (prevents performance problems when it's a long list of possible options)
	onBeforeInputChanges (newValue :any) {
		this.selectedOption = newValue;

		// create options
		this.select._options._results.length = 0;
		if (newValue && newValue[this.optionLabelProperty]) {
			// set value / create option
			let domOption :HTMLElement = document.createElement('ion-option'),
				ionicOption = new Option(new ElementRef(domOption));
			domOption.textContent = newValue[this.optionLabelProperty];
			ionicOption.value = newValue;
			this.select._options._results.push(ionicOption);
		} else {
			// reset / clear options and texts
			if (this.select._texts && this.select._texts.length) {
				this.select._texts.length = 0;
			}
			this.select._text = '';
			this.select.selectedText = '';
		}
	}

}
