import {Component, Input, Output, EventEmitter, ViewChild} from '@angular/core';
import {Searchbar} from 'ionic-angular';

declare let cordova: any;

let tmp = '';

@Component({
	selector: 'cwx-keyboard-searchbar',
	template: `
		<ion-searchbar #search
			[(ngModel)]="value"
			[placeholder]="placeholder"

			(ionInput)="_onInput()"
			(ionCancel)="_onInput()"
			(ionClear)="_onInput()"
			(ionBlur)="hide()"
			(ionFocus)="show()"
		></ion-searchbar>
	`,
	host: {
		//'ion-fixed': '', // commented out because angulars "ngcontent" rule does not apply for attributes set by the "host" property (see "console.error" below)
		'[class.visible]': 'visible'
	}
})
export class CwxKeyboardSearchbar {

	@Input() value :any = tmp;
	@Output() valueChange = new EventEmitter();

	@Input() placeholder :any = '';

	@Output('onInput') onInput = new EventEmitter();
	@Output('onShow') onShow = new EventEmitter();
	@Output('onHide') onHide = new EventEmitter();
	@Output('onSearch') onSearch = new EventEmitter();

	@ViewChild('search') searchbar: Searchbar;

	visible: boolean = false; // nedded to set the "visible" class because browsers don't allow a FOCUS on invisible elements
	private restoreFocus :boolean = false;

	constructor () {}

	ngOnInit () {
		// trigger search on search-icon  click
		this.searchbar.getNativeElement().querySelector('.searchbar-search-icon').onclick = () => {
			this.onSearch.emit(this.value);
		};

		// ENTER key is pressed
		this.searchbar._searchbarInput.nativeElement.addEventListener('keypress', (event) => {
			if (event.which == 13) { // ENTER
				this.onSearch.emit(this.value);
				this.hide();
			}
		});

// todo-dave: on viewenter / -leave of the containing view, to prevent problems with later loaded overlaying views
		if (window['cordova'] && cordova.plugins) {
			cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

			window.addEventListener('native.keyboardhide', () => this._onNativeKeyboardHide()); // eg. for androids native back/down button
		}
	}

	ngOnDestroy () {
		if (window['cordova'] && cordova.plugins) {
			cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);

			window.removeEventListener('native.keyboardhide', this._onNativeKeyboardHide);
		}
	}

	_onInput () {
		this.valueChange.emit(this.value); // two way binding of "value"
		this.onInput.emit(this.value);
	}

	_onNativeKeyboardHide () {
		this.hide();
	}

	show () {
		if (this.restoreFocus) {
			return; // all vars are still in the "show" state
		}

		this.visible = true;

		if (!this.searchbar._isFocus) {
			this.searchbar.setFocus();

			// restore focus if it got accidentally lost in iOS during keyboard slide-up
			this.restoreFocus = true;
			setTimeout(() => this.restoreFocus = false, 500);
		}

		this.onShow.emit();
	}

	hide () {
		// restore focus if it got accidentally lost in iOS during keyboard slide-up
		if (this.restoreFocus) {
			this.searchbar.setFocus();
			return;
		}
		if (this.searchbar._isFocus) {
			this.searchbar._searchbarInput.nativeElement.blur();
		} else if (this.visible) {
			this.visible = false;

			this.onHide.emit();
		}
	}

	setFocus () {
		this.searchbar.setFocus();
	}

}
