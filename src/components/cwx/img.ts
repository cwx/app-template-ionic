import {Component, Input, ElementRef} from '@angular/core';

import {Config} from '../../services/config';

@Component({
	selector: 'cwx-img',
	template: '<img>'
})
export class CwxImg {

	private img :HTMLImageElement;
	private _src :string;

	constructor (
		private element :ElementRef,
		private config :Config
	) {}

	@Input()
	get src(): string {
		return this._src;
	}
	set src(newSrc: string) {
		if (newSrc !== this._src) {
			if (!this.img) {
				this.img = this.element.nativeElement.childNodes[0];
				this.img.src = this.config.blankImage;
			}
			this.img.style.backgroundImage = '';
			this.img.height; // force the browser to clear the image / apply the empty image styling
			this.img.style.backgroundImage = ('url("' + newSrc + '")');
		}
	}

}
