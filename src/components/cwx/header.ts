import {Component, Input, ElementRef, ViewChild, ViewEncapsulation} from '@angular/core';
import {Navbar} from 'ionic-angular';

import {Config} from '../../services/config';

@Component({
	selector: 'cwx-header',
	template: `
		<ion-navbar>
			<button *ngIf="!isModal && !customBack.length" ion-button menuToggle><ion-icon name="menu"></ion-icon></button>
			<ion-title>
				<i class="cwx-icon-{{icon}}" *ngIf="!!icon"></i>
				<div [class.has-subTitle]="subTitle">
					{{title || config.i18n[pageShortName].title}}
					<p *ngIf="subTitle" [innerHTML]="subTitle"></p>
				</div>
			</ion-title>
			<ion-buttons end>
				<ng-content select="[end]:not(button)"></ng-content> <!-- inside "ion-buttons" because I did not find another element that supports the [end] attribute -->
				<ng-content select="button[end]"></ng-content>
				<button *ngIf="!cardIsHidden && !!config.user" ion-button [navPush]="config.pages.card">
					<i class="cwx-icon-credit-card"></i></button>
			</ion-buttons>
		</ion-navbar>
	`,
	styles: [`cwx-header.is-back-fake .back-button { display: block; }`],
	encapsulation: ViewEncapsulation.None, // make styles global
	host: {'[class.is-back-fake]': 'customBack.length'}
})
export class CwxHeader {
	@Input('page-short-name') pageShortName :string;
	@Input() title :string;
	@Input() subTitle :string;
	@Input() icon :string;
	@Input() modal :boolean;
	@Input() extended :boolean; // only used for attribute based styling
	@Input('no-card') noCard :boolean;

	isModal :boolean;
	cardIsHidden :boolean;

	customBack :Array<CwxHeaderCustomBack> = [];

	@ViewChild(Navbar) navBar: Navbar;

	constructor (
		public config :Config,
		private element :ElementRef
	) {}

	ngOnInit () {
		this.isModal = (this.modal === true || (this.element.nativeElement.attributes['modal'] && this.modal !== false));
		this.cardIsHidden = (this.pageShortName == 'card' || this.noCard === true || (this.element.nativeElement.attributes['no-card'] && this.noCard !== false));
	}

	addCustomBack (callback :Function) :CwxHeaderCustomBack {
		let backConfig = {
			callback: callback,
			originalBackAction: this.navBar.backButtonClick,
			originalBackHidden: this.navBar.hideBackButton
		};
		this.customBack.push(backConfig);
		this.navBar.backButtonClick = () => {
			callback.call(this);
			this.removeCustomBack(backConfig);
		};
		this.navBar.hideBackButton = false;
		return backConfig;
	}
	removeCustomBack (backConfig? :CwxHeaderCustomBack) {
		let index;
		if (!backConfig) {
			backConfig = this.customBack.pop();
		} else if ((index = this.customBack.indexOf(backConfig)) !== -1) {
			this.customBack.splice(index, 1);
		}
		this.navBar.backButtonClick = backConfig.originalBackAction;
		this.navBar.hideBackButton = backConfig.originalBackHidden;
	}
}

export class CwxHeaderCustomBack {
	callback :Function;
	originalBackAction :(ev :UIEvent) => void;
	originalBackHidden :boolean;
}