import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {SocialSharing} from "@ionic-native/social-sharing";
import {Diagnostic} from '@ionic-native/diagnostic';
import {Brightness} from '@ionic-native/brightness';

import {Config} from './config';
import {ConfigDataHandler} from './configDataHandler';
import {Http} from './http';
import {Api} from './api';
import {Helpers} from './helpers';
import {Storage} from './storage';

export const Services = {
	list: <Array<any>> [

		// native ------------------------------------------------------------------------------------------------------
		StatusBar,
		SplashScreen,
		SocialSharing,
		Diagnostic,
		Brightness,

		// general -----------------------------------------------------------------------------------------------------
		Config,
		ConfigDataHandler,
		Storage,
		Helpers,
		Http,
		Api

		// project specific --------------------------------------------------------------------------------------------

	]
};
