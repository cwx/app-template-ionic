export class User {
	id :number;
	name :string;
	firstName :string;
	cardnumber :string;
}

export class Language {
	code :string;
	name :string;
	component :any; // i18n.EN
}