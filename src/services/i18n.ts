import {Language} from './models';
import {translations, defaultTranslation, defaultTranslationType} from '../translations/';

export class I18n { // is extended by Config (language saving is handled there)
	defaultLanguage :string = (navigator.language || navigator['userLanguage'] || defaultTranslation.languageKey).substr(0, 2).toLowerCase();
	activeLanguage :string;
	availableLanguages :Array<Language> = [];

	i18n :defaultTranslationType;

	constructor () {
		for (let translation of translations) {
			this.availableLanguages.push({code: translation.languageKey, name: translation.languageLabel, component: translation});
		}
		this.changeLanguage(this.defaultLanguage);
	}

	changeLanguage (languageCode: string) {
		if (!languageCode || languageCode == this.activeLanguage) {
			return;
		}
		let language: Language = this.availableLanguages.filter((language: Language) => language.code == languageCode)[0];
		this.i18n = new language.component();
		this.activeLanguage = languageCode; // is saved in Config via property observer
	}
}
