import {Injectable} from '@angular/core';

import {ObjectObserver} from '../libraries/objectObserver';

import {Config} from './config';
import {Storage} from './storage';
import {Api} from './api';
import {Helpers} from './helpers';

let forceReload :boolean = false; // development only

// initially read stored config
@Injectable()
export class ConfigDataHandler {



	// properties of the CONFIG class to be observed & saved to / restored from storage
	public observedProperties = {
		activeLanguage: {
			save: true,
			storeName: 'language',
			willLoad: (data) => this.config.changeLanguage(data) // do this before value is applied
		},
		userIsDev: {
			save: true
		},
		devMode: {
			save: true,
			set: () => setTimeout(() => this.api.loadMainData(true), 50) // force data from correct server when devMode changed (async because SET is called BEFORE the change)
		},
		devUpdates: {
			save: true
		},
		user: {
			save: true,
			load: (data) => {
				if (data) {
					// load user data (not included in the app template)
				}
			}
		},
		mainDataLoaded: {
			save: true,
			load: false // LOAD is done separately in: "ConfigDataHandler.initiallyReadDataFromStorage"
		},
		mainData: {
			isMain: true,
			save: true
		}
	};



	constructor (
		private config :Config,
		private storage :Storage,
		private api :Api,
		private helpers :Helpers
	) {}

	public initiallyReadDataFromStorage () :Promise<any> {
		let loadMainDataViaApi = false;
		return new Promise((resolveAll) => {
			// load config properties from storage
			let loadPromises = [];
			Promise.all([
				// main data (only)
				new Promise((resolve) =>
					this.helpers.data('mainDataLoaded').then((data :Date) => {
						// the data is loaded AND not too old to be used
						if (data && data.getTime && (data.getTime() > ((new Date()).getTime() - this.config.mainDataExpiry)) && !forceReload) {
							this.config.mainDataLoaded = data;

							for (let propName in this.observedProperties) {
								let propValue = this.observedProperties[propName];
								if (propValue.isMain && (propValue.load || (propValue.save && propValue.load !== false))) {
									loadPromises.push(this.storage.data((propValue.storeName || propName)).then((data) => {
										if (data != null) {
											if (propValue.willLoad && propValue.willLoad.call) {
												propValue.willLoad.call(this, data);
											}
											this.config[propName] = data;
											if (propValue.load && propValue.load.call) {
												propValue.load.call(this, data);
											}
										}
									}));
									if (propValue.loadPromiseContainerParent && propValue.loadPromiseContainerName) {
										propValue.loadPromiseContainerParent[propValue.loadPromiseContainerName] = loadPromises[loadPromises.length - 1];
									}
								}
							}

							resolve();
						}

						// the data is not loaded OR too old to be used -> load
						else {
							loadMainDataViaApi = true;
							resolve();
						}
					})
				),

				// other data (not main)
				new Promise((resolve) => {
					for (let propName in this.observedProperties) {
						let propValue = this.observedProperties[propName];
						if (!propValue.isMain && (propValue.load || (propValue.save && propValue.load !== false))) {
							loadPromises.push(this.storage.data((propValue.storeName || propName)).then((data) => {
								if (data != null) {
									if (propValue.willLoad && propValue.willLoad.call) {
										propValue.willLoad.call(this, data);
									}
									this.config[propName] = data;
									if (propValue.load && propValue.load.call) {
										propValue.load.call(this, data);
									}
								}
							}));
							if (propValue.loadPromiseContainerParent && propValue.loadPromiseContainerName) {
								propValue.loadPromiseContainerParent[propValue.loadPromiseContainerName] = loadPromises[loadPromises.length - 1];
							}
						}
					}

					resolve();
				})
			]).then(() =>
				Promise.all(loadPromises).then(resolveAll)
			);
		}).then(() => {
			if (loadMainDataViaApi) {
				this.startObservation();
				this.api.loadMainData(true);
			} else {
				// process main data (eg. create mappings)
				this.startObservation();
			}
		});
	}

	private startObservation () {
		ObjectObserver.addPropertyChangeListener(this.config, {
			get: (prop) => {
				// special getter listener without the right to fetch and return the value
				if (this.observedProperties[prop] && this.observedProperties[prop].get) {
					return this.observedProperties[prop].get.call(this);
				}
			},
			set: (prop, value) => {
				// special setter listener without the right to change the value
				if (this.observedProperties[prop] && this.observedProperties[prop].set) {
					this.observedProperties[prop].set.call(this, value);
				}

				// save in database
				if (this.observedProperties[prop] && this.observedProperties[prop].save) {
					this.storage.data((this.observedProperties[prop].storeName || prop), value);
				}
			}
		});
	}

}
