import {Injectable} from '@angular/core';

import {Md5} from 'ts-md5/dist/md5';

import {Config} from './config';
import {Http} from './http';
import {User} from './models';

@Injectable()
export class Api {

	constructor (
		private http :Http,
		private config :Config
	) {}

	private loadingMainDataPromise :Promise<void>;
	loadMainData (force :boolean = false) :Promise<void> {
		if (!force) {
			// prevent multiple pending api calls to the server
			if (this.config.loadingMainData) {
				return this.loadingMainDataPromise;
			}

			// the already data is loaded AND not too old to be used -> don't load again yet
			if (this.config.mainDataLoaded && (this.config.mainDataLoaded.getTime() > ((new Date()).getTime() - this.config.mainDataExpiry))) {
				return Promise.resolve();
			}
		}

		this.config.loadingMainData = true;
		this.loadingMainDataPromise = new Promise<void>((resolve, reject) =>
			this.http.getJson(this.config[this.config.devMode ? 'apiBaseUrlDev' : 'apiBaseUrl'] + 'mainData').then(
				(data :{
					success :boolean,
					message :string,
					data :any
				}) => {
					if (data && data.success) {
						// set loaded date to prevent a too high loading interval
						this.config.mainDataLoaded = new Date();

						// map the main data properties to their pendants in CONFIG
						this.config.mainData = data.data;

						this.config.loadingMainData = false;
						resolve();
					} else {
						this.config.loadingMainData = false;
						reject(); // todo: display error message AND resolve to prevent reject-messages
					}
				}
			, reject) // todo: display error message AND resolve to prevent reject-messages
		);

		return this.loadingMainDataPromise;
	}

	login (username :string, password :string) :Promise<{success :boolean, message :string, user: User, isDev :boolean}> {
		return this.http.getJson(this.config[this.config.devMode ? 'apiBaseUrlDev' : 'apiBaseUrl'] + 'login/' + username + '/' + Md5.hashStr(password));
	}

	passwordForgotten (identifier: string) :Promise<void> {
		return this.http.getJson(this.config[this.config.devMode ? 'apiBaseUrlDev' : 'apiBaseUrl'] + 'passwordForgotten/' + identifier);
	}
}
