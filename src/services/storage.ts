import {Injectable} from '@angular/core';
import {Platform} from 'ionic-angular';
import {Storage as IonStorage, StorageConfig as IonStorageConfig} from '@ionic/storage';

import {Config} from './config';

// https://github.com/driftyco/ionic-storage
// quote: Currently the ordering is SQLite, IndexedDB, WebSQL, and LocalStorage

@Injectable()
export class Storage {
	storage :IonStorage;

	private storageConfig:IonStorageConfig = {
		name: Config.storageDatabaseName, // name of the database
		storeName: 'main',
		driverOrder: ['sqlite', 'websql', 'indexeddb', 'localstorage']
	};

	constructor (
		private platform :Platform
	) {
		this.platform.ready().then(() => this.storage = new IonStorage(this.storageConfig));
	}

	data (key :string, value? :any) :Promise<any> {
		// set
		if (value === null) {
			return this.dataRemove(key);
		} else if (value !== undefined) {
			// convert to json anyway to be able to use JSON.parse afterwards (strings and numbers will be quoted)
			try { value = JSON.stringify(value); } catch (err) {}
			return this.storage.set(key, value);
		}

		// get
		else {
			return this.storage.get(key).then((data) => {
				// try parsing it as JSON (strings and numbers will be unquoted)
				try { data = JSON.parse(data); } catch (err) {}

				// convert date
				if (typeof data == 'string' && data.match(/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}/)) {
					data = new Date(data);
				}

				return data;
			});
		}
	}

	dataRemove (key :string) :Promise<any> {
		return this.storage.remove(key);
	}
}
