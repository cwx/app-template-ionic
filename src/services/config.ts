import {Injectable} from '@angular/core';

import {ObjectObserver} from '../libraries/objectObserver';

import {I18n} from './i18n';
import {User} from './models';
import {Pages} from '../pages/_PAGES';

// app / release specific config
let isDebugMode = !!window.location.href.match(/:8100/);

@Injectable()
export class Config extends I18n {

	// manual settings -------------------------------------------------------------------------------------------------
	static storageDatabaseName :string = 'plc_pluscash';

	apiBaseUrl :string = 'https://www.pluscash.ch/api/app/plusCash/';
	apiBaseUrlDev :string = 'https://dev.pluscash.ch/api/app/plusCash/';
	imageBaseUrl :string = 'https://www.pluscash.ch/image/';
	imageBaseUrlDev :string = 'https://dev.pluscash.ch/image/';

	toastDuration :number = 2000;
	mainDataExpiry :number = (120 /* minutes */ * (60 * 1000));

	blankImage :string = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';

	becomeCustomerUrl :string = 'https://www.pluscash.ch/sofunktionierts';

	// social media
	socialMediaBaseUrl :string = 'https://www.pluscash.ch/';
	socialMediaBaseUrlDev :string = 'https://dev.pluscash.ch/';
	socialFacebookFallbackBaseUrl: string = 'https://www.facebook.com/sharer/sharer.php?u=[URL]';
	socialTwitterFallbackBaseUrl: string = 'https://twitter.com/intent/tweet?text=[SUBJECT]&url=[URL]';
	socialGooglePlusFallbackBaseUrl: string = 'https://plus.google.com/share?url=[URL]';
	socialMailFallbackBaseUrl: string = 'mailto:?subject=[SUBJECT]&body=[TEXT]\n\n[URL]';



	// data containers -------------------------------------------------------------------------------------------------
	debugMode :boolean = isDebugMode;
	isRealDevice :boolean; // set in "app.component.ts" because "cordova.plugins" is not set at the time this class is instantiated

	pages :any = Pages.namedList;

	loadingMainData :boolean;
	mainDataLoaded :Date;
	mainData :any;

	userIsDev :boolean = false;
	devMode :boolean = false;
	devUpdates :boolean = false;



	// login -----------------------------------------------------------------------------------------------------------
	user :User | null;



	// constructor -----------------------------------------------------------------------------------------------------
	constructor () {
		super();

		// make this class ready for monitoring changes on properties
		return ObjectObserver.initializeAsObservableObject(this);
	}

}
