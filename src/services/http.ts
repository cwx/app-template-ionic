import {Injectable} from '@angular/core';
import {Http as AngularHttp} from '@angular/http';
import {Events} from 'ionic-angular';

import 'rxjs/add/operator/map';

import {Config} from './config';

@Injectable()
export class Http {
	constructor (
		public http :AngularHttp,
		public config :Config,
		public events :Events
	) {}

	getJson (url:string) :Promise<any> {
		return new Promise((resolve, reject) => {
			this.http.get(url)
				.map((res) => res.json())
				.subscribe(
					(data) => resolve(data),
					(err) => {
						this.events.publish('error:loading', this.config.i18n.error.httpLoad, this.config.i18n.error.dialogTitle, err);
						reject(err);
					}
				);
		});
	}

}
