import {Injectable} from '@angular/core';
import {LoadingController, AlertController, ToastController, ToastOptions, Loading, LoadingOptions, Events} from 'ionic-angular';

import {Md5} from 'ts-md5/dist/md5';
import {Deploy} from '@ionic/cloud-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {Brightness} from '@ionic-native/brightness';
import {SocialSharing} from "@ionic-native/social-sharing";

import {Storage} from './storage';
import {Config} from './config';

type SocialPlatform = 'facebook' | 'twitter' | 'plus' | 'mail';

@Injectable()
export class Helpers {

	constructor (
		private loadingCtrl :LoadingController,
		private alertCtrl :AlertController,
		private toastCtrl :ToastController,
		private storage :Storage,
		private config :Config,
		private deploy: Deploy,
		private splashScreen: SplashScreen,
		private socialSharing :SocialSharing,
		private events: Events,
		private brightness :Brightness
	) {
		this.events.subscribe('error:loading', (msg, title) => {
			this.hideLoader();
			this.alert(msg, title);
		});
	}



	// debug info (for native app without other debugging possibilities) ---------------------------------------------------------------------

	public debugInfo :string = '';
	private debugEl :Element;

	debug (info :string) {
		this.debugInfo += (this.debugInfo ? '<hr>' : '') + info;
		setTimeout(() => {
			if (!this.debugEl) {
				this.debugEl = document.getElementsByClassName('debug-info')[0];
			}
			this.debugEl.scrollTop = this.debugEl.scrollHeight;
		}, 0);
	}



	// loader --------------------------------------------------------------------------------------------------------------------------------

	public loader :Loading;
	public loaderAnimation :Promise<void>;
	public loaderIsVisible :boolean = false;

	showLoader (options? :LoadingOptions) :Promise<void> {
		if (!this.loader) {
			this.loader = this.loadingCtrl.create(options);
			this.loaderIsVisible = true;
			return (this.loaderAnimation = this.loader.present());
		} else if (this.loaderIsVisible) {
			return this.loaderAnimation;
		} else {
			return new Promise<void>((resolve) =>
				this.loaderAnimation.then(() =>
					this.showLoader().then(() =>
						resolve()
					)
				)
			);
		}
	}

	hideLoader () :Promise<void> {
		if (!this.loader) {
			return Promise.resolve();
		} else if (!this.loaderIsVisible) {
			return this.loaderAnimation;
		} else {
			this.loaderIsVisible = false;
			return new Promise<void>((resolve) =>
				this.loaderAnimation.then(() =>
					this.loaderAnimation = this.loader.dismiss().then(() => {
						this.loader = null;
						resolve();
					})
				)
			);
		}
	}



	// alert ---------------------------------------------------------------------------------------------------------------------------------

	alert (message :string, title? :string, config? :{ onDidDismiss? :() => void, buttons? :Array<any>, inputs? :Array<any>, subTitle? :string }) :Promise<void> {
		let alert = this.alertCtrl.create({
			title: title,
			subTitle: config && config.subTitle,
			message: message,
			inputs: config && config.inputs,
			buttons: ((config && config.buttons) || [this.config.i18n.button.ok])
		});
		if (config && config.onDidDismiss) {
			alert.onDidDismiss(config.onDidDismiss);
		}
		return alert.present();
	}

	toast (message :string, config :ToastOptions = {duration: this.config.toastDuration}) {
		config.message = message;
		this.toastCtrl.create(config).present();
	}



	// storage (method mapping - no inheritance to prevent multiple storage instances) -------------------------------------------------------

	data (key :string, value? :any) :Promise<any> {
		return this.storage.data(key, value);
	}

	dataRemove (key :string) :Promise<any> {
		return this.storage.dataRemove(key);
	}



	// update -----------------------------------------------------------------------------------------------------------------------------

	getAppVersion () :Promise<string> {
		return new Promise((resolve) => {
			this.deploy.info().then((info) => {
				if (info.deploy_uuid && info.deploy_uuid.substr(0, 3) != 'NO_') {
					this.deploy.getMetadata(info.deploy_uuid).then(
						(meta) => resolve(meta.version + (meta.channel == 'dev' ? ' - DEV' : '')),
						() => resolve('not available')
					);
				} else {
					resolve(info.binary_version);
				}
			}, () => resolve('not available'));
		});
	}

	checkForUpdates () :Promise<boolean> {
		return new Promise((resolve) => {
			// todo-dave: check for a new app version in the store(s)

			// check for a new content package
			if (this.deploy && this.deploy.channel) {
				this.deploy.channel = (this.config.devUpdates ? 'dev' : 'production');
				this.deploy.check().then((snapshotAvailable: boolean) => {
					if (snapshotAvailable) {
						this.showLoader({content: `<div class="text-center">${this.config.i18n.update.installing}<br>0%</div>`}).then(() =>
							this.deploy.download({onProgress: (percentage :number) => this.setUpdateProgress(percentage / 2)}).then(() =>
								this.deploy.extract({onProgress: (percentage :number) => this.setUpdateProgress(50 + (percentage / 2))}).then(() =>
									this.hideLoader().then(() =>
										this.alert(this.config.i18n.update.done, null, {onDidDismiss: () => {
											this.splashScreen.show();
											this.deploy.load();
										}})
									)
								, () => {
									this.hideLoader();
									resolve(false);
									// todo-dave: message for the user
								})
							, () => {
								this.hideLoader();
								resolve(false);
								// todo-dave: message for the user
							})
						);
						resolve(true);
					} else {
						resolve(false);
					}
				}, () => resolve(false));
			} else {
				resolve(false);
			}
		});
	}

	private setUpdateProgress (percentage: number) {
		this.loader.setContent(`<div class="text-center">${this.config.i18n.update.installing}<br>${Math.round(percentage)}%</div>`);
	}



	// brightness -----------------------------------------------------------------------------------------------------------------------------

	private originalBrightness :number;

	setFullBrightness () {
		this.brightness.getBrightness().then((value) => {
			this.originalBrightness = value;
			this.brightness.setBrightness(1);
// todo-dave: events do not seem to work
			document.addEventListener('pause', this.pauseBrightness);
			document.addEventListener('resume', this.resumeBrightness);
		}, () => { /* prevent unhandled error */ });
	}

	restoreOriginalBrightness () {
		if (this.originalBrightness != null) {
			this.brightness.setBrightness(this.originalBrightness);
			document.removeEventListener('pause', this.pauseBrightness);
			document.removeEventListener('resume', this.resumeBrightness);
		}
	}

	private pauseBrightness () {
		this.brightness.setBrightness(this.originalBrightness);
	}

	private resumeBrightness () {
		this.brightness.setBrightness(1);
	}



	// social media ---------------------------------------------------------------------------------------------------------------------------

	shareOnSocialMedia (platform :SocialPlatform, linkTarget :'partner' | 'offer', targetId :number, subject :string, text :string, tryNative :boolean = true) {
		this.showLoader().then(() => {
			let url = (this.config.devMode ?
					this.config.socialMediaBaseUrlDev :
					this.config.socialMediaBaseUrl)
				+ linkTarget + '/' + targetId;

			if (tryNative) {
				this.socialSharing.shareVia(platform, text, subject, null, url)
					.then(() => this.hideLoader()) // native sharing successful
					.catch(() => this.shareOnSocialMediaFallback(platform, url, subject, text)); // native sharing is not possible => do it via webapi
			} else {
				this.shareOnSocialMediaFallback(platform, url, subject, text);
			}
		});
	}

	private shareOnSocialMediaFallback (platform :SocialPlatform, url :string, subject :string, text :string) {
		let targetUrl = '';
		switch (platform) {
			case 'facebook':
				targetUrl = this.config.socialFacebookFallbackBaseUrl;
				break;
			case 'twitter':
				targetUrl = this.config.socialTwitterFallbackBaseUrl;
				break;
			case 'plus':
				targetUrl = this.config.socialGooglePlusFallbackBaseUrl;
				break;
			case 'mail':
				targetUrl = this.config.socialMailFallbackBaseUrl;
				break;
		}

		targetUrl = targetUrl
			.replace('[URL]', url)
			.replace('[SUBJECT]', subject)
			.replace('[TEXT]', text);
		targetUrl = encodeURI(targetUrl);

		this.hideLoader();

		// for mail should not be opened a new window, because it's a unused empty window
		if (platform == 'mail') {
			window.open(targetUrl, '_self');
		} else {
			window.open(targetUrl);
		}
	}



	// DOM ---------------------------------------------------------------------------------------------------------------------------

	findClosestTag (element :HTMLElement, tagName :string) :HTMLElement {
		while (element.tagName.toLowerCase() != tagName) {
			element = <HTMLElement> element.parentNode;
		}
		return element;
	}

	getContentOffset (element :HTMLElement) :{top :number, left :number} {
		let offset = {top: 0, left: 0},
			offsetParent = element;
		while (offsetParent.className.indexOf('scroll-content') === -1) {
			offset.top += offsetParent.offsetTop;
			offset.left += offsetParent.offsetLeft;
			offsetParent = <HTMLElement> offsetParent.offsetParent;
		}
		return offset;
	}



	// helpers --------------------------------------------------------------------------------------------------------------------------------

	private imagePathMapping :{[id:number] :string} = {}; // string is the path

	getImagePath (id :number, width? :number) :string {
		let key :string = (id + (width ? ('-' + width) : ''));
		if (!this.imagePathMapping[key]) {
			this.imagePathMapping[key] = (this.config[this.config.devMode ? 'imageBaseUrlDev' : 'imageBaseUrl'] + id);
			if (width) {
				let attributes = ('width=' + width);
				this.imagePathMapping[key] += ('/' + attributes + '/' + this.toMd5('camaleo/image/' + id + '/' + attributes + '/#Gäb$5!<Hi4_Ü'));
			}
		}
		return this.imagePathMapping[key];
	}

	getSubArray (array :Array<any>, start :number, length :number) :Array<any> {
		return array.slice(start, (start + length));
	}

	getSassVars (element :HTMLElement, pseudoElement :'before' | 'after' = 'before') {
		let jsonString = (window.getComputedStyle(element, ':' + pseudoElement).getPropertyValue('font-family') || '')
			.replace(/\\"/g, '"').replace(/(^")|("$)|(^')|('$)/g, '');
		try {
			return JSON.parse(jsonString);
		} catch (e) {
			return jsonString;
		}
	}

	toMd5 (value :string) :string {
		return Md5.hashStr(value).toString();
	}

	zeroFill (value :number | string, digits = 2) :string {
		return (Number(value) / Math.pow(10, digits)).toString().substr(2);
	}

}
