import {Component, ViewChild} from '@angular/core';
import {Platform, Nav, Config as IonConfig} from 'ionic-angular';

import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';

import {MenuPushScaleType} from '../libraries/menuType';
MenuPushScaleType.scaling = 0.95;

import {Pages} from '../pages/_PAGES';
import {Config} from '../services/config';
import {ConfigDataHandler} from '../services/configDataHandler';
import {Helpers} from '../services/helpers';
import {Api} from '../services/api';

declare let cordova :any;

@Component({
	templateUrl: 'app.html'
})
export class MyApp {

	rootPage = null;
	private keyboardIsVisible :boolean = false;

	// sideMenu
	@ViewChild(Nav) nav :Nav;
	pages = Pages;

	private initialized :Promise<void> = new Promise<void>((resolve) => this.resolveInitialized = resolve);
	private resolveInitialized :Function;

	constructor (
		private platform :Platform,
		private statusBar: StatusBar,
		private splashScreen: SplashScreen,
		private ionicConfig :IonConfig,
		public config :Config, // Necessary because of correct loading orders in services -> leave here, if used or not
		private configDataHandler :ConfigDataHandler,
		public helpers :Helpers,
		private api :Api
	) {
		// wait for some things to happen before doing the initialization stuff
		this.platform.ready().then(() => // wait for ready before starting other promises because in PROD build cordova plugins are not defined before that
			Promise.all([
				this.configDataHandler.initiallyReadDataFromStorage(), // data must be present when loading a page because of settings and userdata
				this.initialized // wait until "this.nav" is defined (it is as soon as "ngOnInit" fires)
			]).then(() => {
				this.config.isRealDevice = !!(window['cordova'] && window['cordova'].plugins); // set here because "cordova.plugins" is not set at the time "Config" is instantiated
				if (this.config.isRealDevice) {
					// wait for the root page to be loaded before hiding the splashScreen
					let rootPageIsLoadedSubscription = this.nav.viewDidEnter.subscribe(() => {
						rootPageIsLoadedSubscription.unsubscribe(); // make sure it only fires once for the very first root page
						setTimeout(() => this.splashScreen.hide(), 0); // async to make sure the page underneath the splashscreen is fully rendered (if this timeout is missing, the splashscreen sometimes fades to a white page for a split second)

						// status bar
						if (this.platform.is('android')) {
							this.statusBar.overlaysWebView(false);
							// todo-dave: see the following plugins for transparent statusbar in android
							// https://github.com/jeneser/ionic-super-bar // fullscreen (incl. navBar)
							// https://github.com/ekuwang/cordova-plugin-statusbar // transparent (overlay?)
						} else if (this.platform.is('ios')) {
							this.statusBar.overlaysWebView(true);
							this.statusBar.styleDefault(); // black font
							//statusBar.styleBlackTranslucent(); // white font
						}
					});

					// async to improve startup performance / first impression
					setTimeout(() => {
						// prevent unwanted scrolling by the webview when an input focuses
						cordova.plugins.Keyboard.disableScroll(true);

						this.helpers.checkForUpdates();
						this.addDeviceEventListeners();
					}, 0);
				}

				// delay setting the homepage to be sure data from storage is restored before any page is loaded (e.g. for "config.user" to be the real value and not a state that can be overwritten in milliseconds)
				this.rootPage = Pages.rootPage;
			})
		);
	}

	ngOnInit () {
		this.resolveInitialized();
	}

	private addDeviceEventListeners () {
		let contentElement = <HTMLElement> document.getElementsByTagName('ion-app')[0];

		// reload data when resuming to the app (from standby)
		document.addEventListener('resume', () => {
			this.api.loadMainData();
		});

		// shrink / extend webview on keyboard show / hide IF the OS doesn't do it natively
		if (!this.ionicConfig.getBoolean('keyboardResizes', false)) {
			let keyboardHeight :number,
				timeoutId :any;
			window.addEventListener('native.keyboardshow', (event :any) => {
				keyboardHeight = ((event.keyboardHeight || (event.detail && event.detail.keyboardHeight)) || 300); // 300 is for manual trigger fallback while developing
				if (!timeoutId) {
					if (!this.keyboardIsVisible) {
						// if the height is applied too early, our animation will be visible faster then the keyboard
						// the timing depends on the keyboard used (native vs. gboard, ...) and the hardware. maybe 300ms is not always optimal
						timeoutId = setTimeout(() => {
							contentElement.style.height = ((this.platform.height() - keyboardHeight) + 'px'); // set height natively and not via Angular because we don't want the Angular-content-check delay
							timeoutId = null;
						}, 200);
					} else {
						contentElement.style.height = ((this.platform.height() - keyboardHeight) + 'px');
					}
				}
			});
			window.addEventListener('native.keyboardhide', () => {
				clearTimeout(timeoutId);
				timeoutId = null;
				contentElement.style.height = '';
			});
		}

		// keyboard is present
		window.addEventListener('native.keyboardshow', () => {
			if (!this.keyboardIsVisible) {
				contentElement.className += ' keyboardIsVisible'; // set class natively and not via Angular because we don't want the Angular-content-check delay
				this.keyboardIsVisible = true;
			}
		});
		window.addEventListener('native.keyboardhide', () => {
			contentElement.className = contentElement.className.replace(' keyboardIsVisible', '');
			this.keyboardIsVisible = false;
		});
	}

	logout () {
		this.nav.setRoot(this.config.pages.login, {isRoot: true}).then(() => {
			this.config.user = null;
			this.config.userIsDev = false;
			this.config.devMode = false;
		});
	}

}
