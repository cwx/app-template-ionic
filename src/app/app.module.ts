import {NgModule, ErrorHandler} from '@angular/core';
import {IonicApp, IonicModule, IonicErrorHandler} from 'ionic-angular';
import {MyApp} from './app.component';
import {BrowserModule} from '@angular/platform-browser';
import {HttpModule} from '@angular/http';
import {CloudModule} from '@ionic/cloud-angular';

import {Services} from '../services/_SERVICES';
import {Pipes} from '../pipes/_PIPES';
import {Directives} from '../directives/_DIRECTIVES';
import {Components} from '../components/_COMPONENTS';
import {Pages} from '../pages/_PAGES';

@NgModule({
	declarations: [
		MyApp,
		...Pipes.list,
		...Directives.list,
		...Components.list,
		...Components.listNoEntry, // Needed that production builds don't fail
		...Pages.list
	],
	imports: [
		BrowserModule,
		HttpModule,
		IonicModule.forRoot(MyApp, {
			// force iOS
			mode: 'ios',
			iconMode: 'ios',

			// no back button text -> non breaking space from https://www.cs.tut.fi/~jkorpela/chars/spaces.html (engine strips away normal spaces)
			backButtonText: ' ',

			// keyboard auto scroll to input
			scrollAssist: false,
			//autoFocusAssist: false

			spinner: 'crescent'
		}),
		CloudModule.forRoot({
			core: {
				app_id: 'cafc2d26'
			}
		})
	],
	bootstrap: [IonicApp],
	entryComponents: [
		MyApp,
		...Components.list,
		...Pages.list
	],
	providers: [
		{provide: ErrorHandler, useClass: IonicErrorHandler},
		...Services.list
	]
})
export class AppModule {}
