import {Pipe, PipeTransform} from '@angular/core';
import {DatePipe} from '@angular/common';

import {Config} from '../services/config';

@Pipe({name: 'money'})
export class MoneyPipe implements PipeTransform {
	transform (value :any, option? :'forceSign') :string {
		value = parseFloat(value || 0);

		// option: forceSign
		let sign = '';
		if (option == 'forceSign') {
			sign = (value > 0 ? '+ ' : (value < 0 ? '- ' : ''));
			value = Math.abs(value);
		}

		// money format conversion
		let money = value.toFixed(2),
			parts = money.split('.');
		money = (parts[1] == '00' ? (parts[0] + '.-') : money);

		return (sign + money);
	}
}

@Pipe({name: 'localDate'})
export class LocalDatePipe implements PipeTransform {
	constructor (
		private config :Config
	) {}

	transform (value: any, pattern?: string) :string | null {
		return (new DatePipe(this.config.i18n.languageCode)).transform(value, pattern);
	}
}