import {LocalDatePipe, MoneyPipe} from './pipes';

export const Pipes = {
	list: <Array<any>> [

		MoneyPipe,
		LocalDatePipe

	]
};