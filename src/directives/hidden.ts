import {Directive, ElementRef, Input, Output, EventEmitter} from '@angular/core';

@Directive({
	selector: '[hidden]'
})
export class Element_Hidden {
	@Input()
	set hidden(value :any) {
		// angular removes the attribute and handles the binding internally, therefore we apply / remove it again
		if (!!value) {
			this.element.nativeElement.setAttribute('hidden', true);
			this.elHide.emit();
		} else {
			this.element.nativeElement.removeAttribute('hidden');
			this.elShow.emit();
		}

		this.elToggle.emit({value: !value});
	}

	@Output('el-show') elShow = new EventEmitter();
	@Output('el-hide') elHide = new EventEmitter();
	@Output('el-toggle') elToggle = new EventEmitter();

	constructor (
		private element :ElementRef
	) {}
}
