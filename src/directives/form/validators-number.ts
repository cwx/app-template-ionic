import {Directive, forwardRef, Input} from '@angular/core';
import {Validator, NG_VALIDATORS, FormControl} from '@angular/forms';

@Directive({
	selector: '[min][formControlName], [min][formControl], [min][ngModel]',
	providers: [
		{provide: NG_VALIDATORS, useExisting: forwardRef(() => FormValidator_Min), multi: true}
	]
})
export class FormValidator_Min implements Validator {
	@Input() min :string;

	validate (c :FormControl) :{validateMin :boolean} {
		return ((this.min !== undefined && c.value != '' && Number(c.value) < Number(this.min)) ? {validateMin: false} : null);
	}
}

@Directive({
	selector: '[max][formControlName], [max][formControl], [max][ngModel]',
	providers: [
		{provide: NG_VALIDATORS, useExisting: forwardRef(() => FormValidator_Max), multi: true}
	]
})
export class FormValidator_Max implements Validator {
	@Input() max :string;

	validate (c :FormControl) :{validateMax :boolean} {
		return ((this.max !== undefined && c.value != '' && Number(c.value) > Number(this.max)) ? {validateMax: false} : null);
	}
}
