import {FormValidator_Max, FormValidator_Min} from './form/validators-number';
import {Element_Hidden} from './hidden';

export const Directives = {
	list: <Array<any>> [

		// general -----------------------------------------------------------------------------------------------------
		Element_Hidden,

		// form: number ------------------------------------------------------------------------------------------------
		FormValidator_Max,
		FormValidator_Min

		// project specific --------------------------------------------------------------------------------------------

	]
};
