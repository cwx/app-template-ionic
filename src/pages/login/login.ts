import {Component} from '@angular/core';
import {NavParams, NavController} from 'ionic-angular';

import {Api} from '../../services/api';
import {Helpers} from '../../services/helpers';
import {Config} from '../../services/config';

@Component({
	selector: 'page-login',
	templateUrl: 'login.html'
})
export class LoginPage {

	hasBackButton: boolean;
	hasSkipButton: boolean;

	username: string;
	password: string;
	initializing :boolean = true;
	requestPending :boolean = false;
	passwordVisible :boolean = false;
	passwordForgotten :boolean = false;
	passwordForgottenIdentifier: string;

	constructor (
		public nav :NavController,
		private navParams :NavParams,
		private api :Api,
		private helpers :Helpers,
		public config :Config
	) {
		this.hasSkipButton = !this.navParams.get('skipDisabled');
	}

	ngOnInit () {
		this.hasBackButton = !(this.navParams.get('isRoot') || !this.nav.getPrevious()); // if this page is not stacked there's nothing to go back to (not in constructor because "nav.getPrevious()" always returns undefined there)
	}

	ionViewDidEnter () {
		this.initializing = false; // make sure the first login stop is displayed while slide is entering when user pressed "logout" and the data is not cleared, yet
	}

	onLogin () {
		this.requestPending = true;
		this.api.login(this.username, this.password).then((data) => {
			this.requestPending = false;

			// break when serverside authentication check failed
			if (!data.success) {
// todo-dave: i18n for data.message which comes from the server in german (translate on the server and pass on the language?)
				this.helpers.alert(data.message, this.config.i18n.error.dialogTitle);
			}

			// save data into storage
			else {
				this.config.user = data.user;
				if (data.isDev) {
					this.config.userIsDev = true;
					this.config.devMode = true; // devMode is on by default for dev users
				}
				// load user data (not included in the app template)
			}
		}, () => { /* prevent unhandled error */ });
	}

	onSendPassword () {
		this.requestPending = true;
		this.api.passwordForgotten(this.passwordForgottenIdentifier).then(() => {
			this.requestPending = false;
			this.passwordForgotten = false;
			this.helpers.toast(this.config.i18n.login.passwordForgotten.success);
		}, () => { /* prevent unhandled error */ });
	}

	dismiss () {
		if (this.passwordForgotten) {
			this.passwordForgotten = false;
		} else if (this.hasBackButton) {
			this.nav.pop();
		} else {
			this.nav.setRoot(this.config.pages.home, {}, {animate: true});
		}
	}

}
