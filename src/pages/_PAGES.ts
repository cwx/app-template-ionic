import {HomePage} from './home/home';
import {LoginPage} from './login/login';
import {SettingsPage} from './settings/settings';

// necessary because the compiler has to know what is there before it runs the JS (FOR loop below)
let rawList = [
	HomePage,
	LoginPage,
	SettingsPage
];

let list = [
	{shortName: 'home', component: HomePage, icon: 'logo'},
	{shortName: 'login', component: LoginPage},
	{shortName: 'settings', component: SettingsPage}
];

let sideMenu = [
	'home'
];

export const Pages = {
	rootPage: HomePage,

	list: rawList,
	namedList: <{[shortName :string] : any}> [], // any is the component
	sideMenu: <Array<{shortName: string, component: any, icon? :string}>> []
};

// fill named list
for (let page of list) {
	Pages.namedList[page.shortName] = page.component;
}

// fill sideMenu
for (let name of sideMenu) {
	Pages.sideMenu.push(list.filter((item) => item.shortName == name)[0]);
}
