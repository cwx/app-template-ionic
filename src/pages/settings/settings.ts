import {Component} from '@angular/core';

import {Config} from '../../services/config';
import {Language} from '../../services/models';
import {Helpers} from '../../services/helpers';

@Component({
	selector: 'page-settings',
	templateUrl: 'settings.html'
})
export class SettingsPage {

	version :string;
	checkingForUpdates :boolean = false;

	constructor (
		public config :Config,
		private helpers :Helpers
	) {
		// order list of available languages alphabetically by name
		this.config.availableLanguages.sort((a:Language, b:Language) => (a.name.toLowerCase() > b.name.toLowerCase() ? 1 : -1));

		// get version of the app
		this.helpers.getAppVersion().then((data) => this.version = data, () => { /* prevent unhandled error */ });
	}

	checkForUpdates () {
		this.checkingForUpdates = true;
		this.helpers.checkForUpdates().then((updatesAvailable) => {
			if (!updatesAvailable) {
				this.helpers.toast(this.config.i18n.update.isLatest);
			}
			this.checkingForUpdates = false;
		});
	}

}
