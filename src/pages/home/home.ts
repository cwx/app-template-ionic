import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';

import {Config} from '../../services/config';

@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})
export class HomePage {
	constructor (
		public nav: NavController,
		public config :Config
	) {
		// debugging -> only active during development
		if (this.config.debugMode) {
			//nav.push(config.pages.login, {isSkipLoginPossible: true, isHidingBackButton: true}); // login at startup
			//nav.setRoot(config.pages.settings);
		}
	}
}
