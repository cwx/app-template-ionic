export class EN {
	static languageKey = 'en';
	static languageLabel = 'English';
	languageCode = 'en-US';

	button = {
		ok: 'Ok',
		cancel: 'Cancel',
		yes: 'Yes',
		no: 'No',
		login: 'Login',
		done: 'Done',
		next: 'Next'
	};

	error = {
		dialogTitle: 'Error!',
		httpLoad: 'Error loading the data.<br><br>The server is eventually temporarily offline, please try again later.'
	};

	update = {
		installing: 'Installing new content package',
		done: 'Content package successfully installed.<br>The app will restart now.',
		version: 'Version',
		devChannel: 'Developer updates',
		check: 'Check for updates',
		isLatest: 'The app is up to date'
	};

	// pages
	home = {
		title: 'Homepage',
		menuTitle: 'Home',
		loadingMainData: 'loading data ...'
	};
	login = {
		title: 'Login',
		logout: 'Logout',
		loggedInNameTitle: 'Logged in as',
		username: 'Username',
		password: 'Password',
		register: 'Create a login',
		skip: 'Skip login',
		seePassword: 'See password',
		passwordForgotten: {
			text: 'I forgot my password',
			intro: 'Enter your email address you registered with. We will send you an email with your password.',
			hint: 'Please be aware that we cannot tell you if your email address is correct, for security reasons. Try again if the mail does not arrive.',
			identifier: 'Email address',
			send: 'Send password',
			success: 'Your password was sent.'
		}
	};
	settings = {
		title: 'Settings',
		language: 'Language',
		devMode: 'Developer mode'
	};
}