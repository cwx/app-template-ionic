export class DE implements EN {
	static languageKey = 'de';
	static languageLabel = 'Deutsch';
	languageCode = 'de-DE';

	button = {
		ok: 'Fertig',
		cancel: 'Abbrechen',
		yes: 'Ja',
		no: 'Nein',
		login: 'Anmelden',
		done: 'Fertig',
		next: 'Weiter'
	};

	error = {
		dialogTitle: 'Fehler!',
		httpLoad: 'Beim Abrufen der Daten ist ein Fehler aufgetreten.<br><br>Eventuell ist der Server nicht erreichbar, versuchen Sie es später noch einmal.'
	};

	update = {
		installing: 'Inhalte werden aktualisiert',
		done: 'Inhalte erfolgreich aktualisiert.<br>Die App wird neu gestartet.',
		version: 'Version',
		devChannel: 'Entwickler-Updates',
		check: 'Auf Updates prüfen',
		isLatest: 'Die App ist bereits aktuell'
	};

	// pages
	home = {
		title: 'Homepage',
		menuTitle: 'Home',
		loadingMainData: 'lade Daten ...'
	};
	login = {
		title: 'Einloggen',
		logout: 'Ausloggen',
		loggedInNameTitle: 'Eingeloggt als',
		username: 'Benutzername',
		password: 'Passwort',
		register: 'Noch kein Login?',
		skip: 'Weiter ohne Login',
		seePassword: 'Passwort ansehen',
		passwordForgotten: {
			text: 'Passwort vergessen',
			intro: 'Gib deine Emailadresse ein, mit der du dich registriert hast. Wir senden dir dann eine Email mit dem Passwort.',
			hint: 'Bitte beachte, dass wir dir aus Sicherheitsgründen nicht sagen können ob die Emailadresse korrekt ist. Wenn das Email nicht ankommt, versuche es nochmal.',
			identifier: 'Emailadresse',
			send: 'Passwort zusenden',
			success: 'Das Passwort wurde gesendet.'
		}
	};
	settings = {
		title: 'Einstellungen',
		language: 'Sprache',
		devMode: 'Entwickler-Modus'
	};
}

import {EN} from './en';