// add new translation here AND ...
import {EN} from './en';
import {DE} from './de';

// ... and add new translation here
export const translations = [
	EN, DE
];



export type defaultTranslationType = EN;
export const defaultTranslation = EN;
