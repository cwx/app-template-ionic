interface Listener {
	get? :(property :string) => void;
	set? :(property :string, value :any) => void;
}

export class ObjectObserver {

	private static listenerStore :Array<{
		proxy :any; // todo-dave: change return value to "Proxy" as soon as ES6 is the target - now it's ES5
		listeners :Array<Listener>;
	}> = [];

	public static addPropertyChangeListener (observedObject :any, listener :Listener) :void {
		if (observedObject.___objectObserverId >= 0) {
			this.listenerStore[observedObject.___objectObserverId].listeners.push(listener);
		} else {
			console.log('ObjectObserver', 'object not initialized', observedObject);
		}
	}

	public static initializeAsObservableObject (observedObject :any) :any { // todo-dave: change return value to "Proxy" as soon as ES6 is the target - now it's ES5
		if (observedObject.___objectObserverId >= 0) {
			return this.listenerStore[observedObject.___objectObserverId].proxy;
		}
		observedObject.___objectObserverId = this.listenerStore.length;

		let proxy = new Proxy(observedObject, {
			get: (obj, prop) => {
				for (let listener of this.listenerStore[obj.___objectObserverId].listeners) {
					if (listener.get && listener.get.apply) {
						listener.get.call(obj, prop);
					}
				}

				return prop in obj ? obj[prop] : undefined;
			},
			set: (obj, prop, value) => {
				for (let listener of this.listenerStore[obj.___objectObserverId].listeners) {
					if (listener.set && listener.set.apply) {
						listener.set.call(obj, prop, value);
					}
				}

				// The default behavior to store the value
				obj[prop] = value;

				// Indicate success
				return true;
			}
		});

		this.listenerStore.push({
			proxy: proxy,
			listeners: []
		});

		return proxy;
	}

}



// Proxy polyfill for older browsers
if (!window['Proxy']) {
	window['Proxy'] = function (observedObject :any, config :any) {
		if (!config || !config.set || !config.set.call) {
			console.warn('No SET method specified for Proxy polyfill. GETters are not supported!');
			return;
		}

		function stringify (source) {
			let cache = [],
				json = JSON.stringify(source, function (key, value) {
					if (typeof value == 'object') {
						if (cache.indexOf(value) !== -1) {
							return;
						}
						cache.push(value);
					}
					return value;
				});
			cache = null; // garbage collection
			return json;
		}

		let jsonRepresentation = {
			whole: stringify(observedObject),
			props: {}
		};

		function convertPropVal (value) {
			return (typeof value == 'object' ? stringify(value) : value);
		}

		for (let key in observedObject) {
			jsonRepresentation.props[key] = convertPropVal(observedObject[key]);
		}

		function checkObject () {
			clearTimeout(checkTimeoutId);

			let whole = stringify(observedObject);

			if (jsonRepresentation.whole != whole) {
				for (let key in observedObject) {
					let value = convertPropVal(observedObject[key]);
					if (value != jsonRepresentation.props[key]) {
						jsonRepresentation.props[key] = value;
						config.set(observedObject, key, observedObject[key]);
					}
				}
				jsonRepresentation.whole = whole;
			}

			startCheck();
		}
		observedObject.doProxyChangeDetection = function () {
			checkObject();
		};

		let checkTimeoutId :any;
		function startCheck () {
			checkTimeoutId = setTimeout(checkObject, 500);
		}

		startCheck();

		return observedObject;
	};
}