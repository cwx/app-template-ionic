import {Animation, Menu, MenuController, MenuType, Platform} from 'ionic-angular';

export class MenuPushScaleType extends MenuType {
	static scaling :number = 0.95;

	constructor(menu: Menu, plt: Platform) {
		super(plt);

		let contentOpenedX: string,
			menuClosedX: string,
			menuOpenedX: string;

		const width = menu.width();
		if (menu.isRightSide) {
			// right side
			contentOpenedX = -width + 'px';
			menuClosedX = width + 'px';
			menuOpenedX = '0px';
		} else {
			contentOpenedX = width + 'px';
			menuOpenedX = '0px';
			menuClosedX = -width + 'px';
		}

		const menuAni = new Animation(plt, menu.getMenuElement());
		menuAni.fromTo('translateX', menuClosedX, menuOpenedX);
		this.ani.add(menuAni);

		const contentApi = new Animation(plt, menu.getContentElement());
		contentApi.fromTo('translateX', '0px', contentOpenedX);
		contentApi.fromTo('scale', 1, MenuPushScaleType.scaling);
		contentApi.fromTo('margin-left', '0%', ('-' + ((100 - (MenuPushScaleType.scaling * 100)) / 2) + '%'));
		this.ani.add(contentApi);
	}
}
MenuController.registerType('pushScale', MenuPushScaleType);