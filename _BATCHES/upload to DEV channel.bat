@echo off
SET version="FEHLER - Schliesse das Programm manuell"
for /f %%i in ('_xpath.bat "..\config.xml" "//widget/@version"') do set version=%%i
echo Version (in config.xml): %version%
SET note=
SET /P note="Notiz zur Version: "
CD ..
@echo on
CALL ionic upload --deploy=dev --note="%note%" --metadata="{\"channel\":\"dev\",\"version\":\"%version%\"}"
PAUSE